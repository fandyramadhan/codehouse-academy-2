import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // add
  TextEditingController nameController = new TextEditingController();
  TextEditingController penulisController = new TextEditingController();
  TextEditingController tahunController = new TextEditingController();
  TextEditingController jumlahController = new TextEditingController();

  // edit
  TextEditingController nameEditController = new TextEditingController();
  TextEditingController penulisEditController = new TextEditingController();
  TextEditingController tahunEditController = new TextEditingController();
  TextEditingController jumlahEditController = new TextEditingController();

  List<Map<String, dynamic>> persons = [
    {
      "name": "Buku Matematika",
      "penulis": "Guru MTK",
      "tahun": "2008",
      "jumlah": "12",
    },
    {
      "name": "Buku IPA",
      "penulis": "Guru IPA",
      "tahun": "2001",
      "jumlah": "12",
    },
    {
      "name": "Buku IPS",
      "penulis": "Guru IPS",
      "tahun": "2018",
      "jumlah": "12",
    },
  ];

  void _showDialogAdd() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Tambah Buku"),
            content: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextFormField(
                  controller: nameController,
                  decoration: InputDecoration(labelText: "Nama Buku"),
                ),
                SizedBox(
                  height: 3,
                ),
                TextFormField(
                  controller: penulisController,
                  decoration: InputDecoration(labelText: "Nama Penulis"),
                ),
                SizedBox(
                  height: 3,
                ),
                TextFormField(
                  controller: tahunController,
                  decoration: InputDecoration(labelText: "Tahun Terbit"),
                ),
                SizedBox(
                  height: 3,
                ),
                TextFormField(
                  controller: jumlahController,
                  decoration: InputDecoration(labelText: "Jumlah"),
                ),
              ],
            ),
            actions: [
              FlatButton(
                  onPressed: () {
                    String name = nameController.text;
                    String penulis = penulisController.text;
                    String tahun = tahunController.text;
                    String jumlah = jumlahController.text;
                    persons.add({
                      "name": name,
                      "penulis": penulis,
                      "tahun": tahun,
                      "jumlah": jumlah,
                    });
                    nameController.text = "";
                    penulisController.text = "";
                    tahunController.text = "";
                    jumlahController.text = "";
                    setState(() {});
                    Navigator.of(context).pop();
                  },
                  child: Text("Add")),
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("Cancel"))
            ],
          );
        });
  }

  void _showDialogEdit(int index, Map<String, dynamic> person) {
    nameEditController.text = person["name"];
    penulisEditController.text = person["penulis"];
    tahunEditController.text = person["tahun"];
    jumlahEditController.text = person["jumlah"];
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Edit Buku"),
            content: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextFormField(
                  controller: nameEditController,
                  decoration: InputDecoration(labelText: "Nama Buku"),
                ),
                SizedBox(
                  height: 3,
                ),
                TextFormField(
                  controller: penulisEditController,
                  decoration: InputDecoration(labelText: "Penulis Buku"),
                ),
                SizedBox(
                  height: 3,
                ),
                TextFormField(
                  controller: tahunEditController,
                  decoration: InputDecoration(labelText: "Tahun Terbit"),
                ),
                SizedBox(
                  height: 3,
                ),
                TextFormField(
                  controller: jumlahEditController,
                  decoration: InputDecoration(labelText: "Jumlah"),
                ),
              ],
            ),
            actions: [
              FlatButton(
                  onPressed: () {
                    String name = nameEditController.text;
                    String penulis = penulisEditController.text;
                    String tahun = tahunEditController.text;
                    String jumlah = jumlahEditController.text;

                    persons[index]["name"] = name;
                    persons[index]["penulis"] = penulis;
                    persons[index]["tahun"] = tahun;
                    persons[index]["jumlah"] = jumlah;
                    nameEditController.text = "";
                    penulisEditController.text = "";
                    tahunEditController.text = "";
                    jumlahEditController.text = "";
                    setState(() {});
                    Navigator.of(context).pop();
                  },
                  child: Text("Edit")),
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("Cancel"))
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Buku Perpustakaan'),
        flexibleSpace: Container(
          color: Colors.indigo[800],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _showDialogAdd,
        icon: Icon(Icons.add),
        label: Text('Tambah Buku'),
        backgroundColor: Colors.indigo,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: Container(
        padding: EdgeInsets.all(5),
        child: ListView.separated(
            separatorBuilder: (BuildContext context, int index) {
              return Divider(
                color: Colors.black,
              );
            },
            itemCount: persons.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                  contentPadding: EdgeInsets.all(20),
                  isThreeLine: true,
                  // leading: Icon(
                  //   Icons.book_rounded,
                  //   size: 30.0,
                  //   color: Colors.blue[400],
                  // ),
                  trailing: Container(
                    width: 70,
                    child: Row(
                      children: [
                        InkWell(
                          onTap: () {
                            setState(() {
                              persons.removeAt(index);
                            });
                          },
                          child: Icon(Icons.delete),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        InkWell(
                          onTap: () {
                            _showDialogEdit(index, persons[index]);
                          },
                          child: Icon(Icons.edit),
                        ),
                      ],
                    ),
                  ),
                  title: Text(
                    persons[index]["name"],
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  subtitle: Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Penulis'),
                            Text(
                              persons[index]["penulis"],
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Tahun'),
                            Text(
                              persons[index]["tahun"],
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Jumlah'),
                            Text(
                              persons[index]["jumlah"],
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ));
            }),
      ),
    );
  }
}
